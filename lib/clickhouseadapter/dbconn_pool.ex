defmodule Clickhouseadapter.DBConn do


  @timeout 60_000
  def timeout() do
    @timeout
  end


  @type t :: %{
        name:      iodata,
        statement: iodata,
        columns:   [String.t] | nil
    }

  def start_link(opts) do
    DBConnection.start_link(Clickhouseadapter.Connection, opts)
  end


  def child_spec(opts) do
    DBConnection.child_spec(Clickhouseadapter.Connection, opts)
  end



  def prep_execute(conn, statement, params, opts \\ []) do

    DBConnection.prepare_execute(conn, %Clickhouseadapter.Query{name: "", statement: statement}, params, opts)
  end


  defp format_query(query, format) do

    query <> "FORMAT" <> format
  end


  # Here DBConn logic
end
