defmodule Clickhouseadapter.Error do
  defstruct   [
    :message,
    :code,
    :constraint_violations
  ]
end
