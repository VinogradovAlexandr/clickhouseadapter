defmodule Clickhouseadapter.HTTP do


  @selected_queries_regex ~r/^(SELECT|SHOW|DESCRIBE|EXISTS)/i # FIXME EXISTS was here

  @req_headers [{"Content-Type", "application/json"}]
  def init_http() do
    HTTPoison.start
  end

  def http_get_request(base_address, query) do

    res = HTTPoison.get(base_address <> query)


  end

  def http_post_request(base_address, query) do

    res = HTTPoison.post(base_address , query, @req_headers)


  end

  def http_request(method , base_address, query, opt \\ []) do


    HTTPoison.request(method, base_address, query, @req_headers, opt)
  end


  def send(query, base_address, timeout, username, password, database) when username != nil do
    opts = [hackney: [basic_auth: {username, password}], timeout: timeout, recv_timeout: timeout]


    head_query = hd(query)

    command = parse_command(head_query)

    command
    |> Kernel.inspect
    |> IO.puts

    res = http_post_request(base_address , head_query)

    case res do
      {:ok, resp} ->
        cond do
          resp.status_code == 200 ->
            case command do
              :selected ->
                # TODO JSON instead of Poison
                case Poison.decode(resp.body) do
                  {:ok, %{"meta" => meta, "data" => data, "rows" => _rows_count}} ->
                    columns = meta |> Enum.map(fn(%{"name" => name, "type" => _type}) -> name end)
                    rows = data |> Enum.map(fn(data_row) ->
                      meta
                      |> Enum.map(fn(%{"name" => column, "type" => column_type}) ->
                        Clickhouseadapter.Types.decode(data_row[column], column_type)
                      end)
                      |> List.to_tuple()
                    end)

                    {command, columns, rows}
                    {:error, reason} -> {:error, reason}
          end

        :updated ->
            {:updated, 1}
        :created ->
          {:created, 1}
      end
      true ->{:error, resp.body}

    end
    {:error, error} -> {:error, error.reason}
  end
end


  defp parse_command(query) do
      cond do
        query =~ @selected_queries_regex -> :selected

        true -> :updated
      end
    end

end
