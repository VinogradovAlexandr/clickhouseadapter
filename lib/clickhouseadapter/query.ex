defmodule Clickhouseadapter.Query do


  alias Clickhouseadapter.QueryHelper

  defstruct [:name, :statement, :columns]

  def execute_query(query, params, opts, state) do
    # {:ok, query, %Clickhouseadapter.Result{
    #         command: :ok,
    #         columns: nil,
    #         rows: [[42, 21]],
    #         num_rows: 1
    #       }, state}


    base_address = state.base_address
    username = state.conn_opts[:username]
    password = state.conn_opts[:password]
    timeout = state.conn_opts[:timeout]
    database = state.conn_opts[:database]

    sql_query = query.statement |> IO.iodata_to_binary() |> split_query(params)
    res = sql_query |> Clickhouseadapter.HTTP.send(base_address, timeout, "username", password, database)

    case res do
      {:error, %Clickhouseadapter.Error{code: :connection_exception} = reason} ->
        {:disconnect, reason, state}

      {:error, reason} ->
        {:error, reason, state}


        {:selected, columns, rows} ->

          {:ok, query, %Clickhouseadapter.Result{
                  command: :selected,
                  columns: columns,
                  rows: rows,
                  num_rows: Enum.count(rows)
                }, state}
      {:updated, count} ->

        {:ok, query, %Clickhouseadapter.Result{
                command: :updated,
                columns: ["count"],
                rows: [[count]],
                num_rows: 1
              }, state}




      {command, columns, rows} ->
        {
          :ok,
          %Clickhouseadapter.Result{
            command: command,
            columns: columns,
            rows: rows,
            num_rows: Enum.count(rows)
          },
          state
        }
    end
  end



  defp split_query(query, params) do
    # TODO case errors
    String.split(query, "?")
  end

  defimpl DBConnection.Query, for: Clickhouseadapter.Query do
  #require IEx

    def parse(query, opts) do
      query
    end

    def describe(query, opts) do
      query
    end
    def encode(query, params, opts) do
      params
    end

    def decode(_query, result, _opts) do
        case result.command do
            :selected ->
                rows = result.rows
                  new_rows = Enum.map(rows, fn el ->
                      list1 = Tuple.to_list(el)
                        Enum.map(list1, fn el1 ->
                            cond do
                                is_list(el1) ->
                  to_string(el1)
                    true ->
                  el1
                  end
                  end)
                  end)
                    Map.put(result, :rows, new_rows)
     _               ->
       result
   end
 end



 end

end
