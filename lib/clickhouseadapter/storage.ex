defmodule Clickhouseadapter.Storage do
  alias Clickhouseadapter.HTTP
  @behaviour Ecto.Adapter.Storage

  def storage_down(opts) do
    # drop the storage given by options
    databases = Map.fetch!(opts, :database) || raise "Database name is nil"
    query = ~s[DROP DATABASE IF EXISTS "#{databases}"]
    HTTP.http_post_request("http://localhost:8123/",query)
  end


  def storage_up(opts) do
    # create the storage given by options
    databases = Map.fetch!(opts, :database)
    query = ~s[CREATE DATABASE IF NOT EXISTS "#{databases}"]
    HTTP.http_post_request("http://localhost:8123/",query)

  end

end
