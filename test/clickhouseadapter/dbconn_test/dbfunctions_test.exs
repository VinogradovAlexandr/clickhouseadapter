defmodule Clickhouseadapter.DBConnTest do

  use ExUnit.Case
  alias Clickhouseadapter.Result


  setup_all do

    {:ok, conn} = Clickhouseadapter.DBConn.start_link([show_sensitive_data_on_connection_error: true])
    Clickhouseadapter.Storage.storage_down(%{database: "query_test"})
    Clickhouseadapter.Storage.storage_up(%{database: "query_test"})
    {:ok, [conn: conn]}
  end

  test "db insert", %{conn: conn} do
    assert {:ok, _, %Result{}}
           = Clickhouseadapter.DBConn.prep_execute(conn, "CREATE TABLE IF NOT EXISTS query_test.simple_select (name String) ENGINE = Memory", [])

    assert {:ok, _, %Result{command: :updated, num_rows: 1}}
           = Clickhouseadapter.DBConn.prep_execute(conn, ["INSERT INTO query_test.simple_select VALUES ('qwerty')" ], [])
    assert {:ok, _, %Result{command: :selected, columns: ["name"], num_rows: 1, rows: [["qwerty"]]}}
         = Clickhouseadapter.DBConn.prep_execute(conn, "SELECT * FROM query_test.simple_select" <> " FORMAT JSON", [])
  end
  







end
