defmodule Clickhouseadapter.HTTP_test do

  use ExUnit.Case

  test "GET request to ClickHouse", %{} do
    {:ok, resp} = Clickhouseadapter.HTTP.http_get_request("http://localhost:8123/?query=", "SELECT%201")

    assert resp.body  =~ "1"
  end

  test "POST request", %{} do
    {:ok, resp} = Clickhouseadapter.HTTP.http_post_request("http://localhost:8123/", "SELECT 1")

    assert resp.body  =~ "1"
  end


  test "http_request", %{} do
    {:ok, resp} = Clickhouseadapter.HTTP.http_request(:post, "http://localhost:8123/", "SELECT 1")

    assert resp.body  =~ "1"
    
  end


end
