defmodule Clickhouseadapter.QueryTest do
  use ExUnit.Case
  alias Clickhouseadapter.QueryHelper
  import Ecto.Query
  defmodule Schema do
      use Ecto.Schema

      schema "schema" do
        field :x, :integer
        field :y, :integer
        field :z, :integer
      end
    end


  setup_all do
    {:ok, conn} = Clickhouseadapter.DBConn.start_link([show_sensitive_data_on_connection_error: true])
    Clickhouseadapter.Storage.storage_down(%{database: "query_test"})
    Clickhouseadapter.Storage.storage_up(%{database: "query_test"})
    {:ok, [conn: conn]}
  end

  test "SELECT FROM TEST", %{} do
    query = Schema |> group_by([p], p.room_name) |> select([p], p.room_name)
    # Enum.map([:prefix, :sources, :from, :joins, :aliases, :wheres, :select,
    #           :order_bys, :limit, :offset, :group_bys, :combinations, :updates,
    #          :havings, :preloads, :assocs, :distinct, :lock, :windows], fn x -> Map.get(query, x)  |>IO.inspect end)

    # FIXME Maybe wrong query

    # defstruct [prefix: nil, sources: nil, from: nil, joins: [], aliases: %{}, wheres: [], select: nil,
    #          order_bys: [], limit: nil, offset: nil, group_bys: [], combinations: [], updates: [],
    #          havings: [], preloads: [], assocs: [], distinct: nil, lock: nil, windows: []]



    assert QueryHelper.all(query) == 1
  end
end
